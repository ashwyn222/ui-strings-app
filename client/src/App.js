import React from 'react';
import './App.css';
import FirstComponent from './components/firstComponent';

class App extends React.Component {
  
 

  render() {
    return (
      <div className="app">
        <FirstComponent></FirstComponent>
      </div>
    );
  }
}

export default App;